import supertest from 'supertest'

import app from './server'
const request = supertest(app)


it('Call the /nusantech endpoint', async done => {
    const res = await request.get('/nusantech')
    expect(res.status).toBe(200)
    expect(res.text).toBe('Hello, Nusantech!')
    done()
})
it('Call the / endpoint', async done => {
    const res = await request.get('/')
    expect(res.status).toBe(200)
    expect(res.text).toBe('This App is running properly!')
    done()
})
it('Call the /pong endpoint', async done => {
    const res = await request.get('/ping')
    expect(res.status).toBe(200)
    expect(res.text).toBe('Pong!')
    done()
})
it('Call the /hello/:name endpoint', async done => {
    const res = await request.get('/hello/ulfa')
    expect(res.status).toBe(200)
    expect(res.body.message).toBe('Hello Ulfa')
    done()
})
it('Call the /ulfa endpoint', async done => {
    const res = await request.get('/ulfa')
    expect(res.status).toBe(200)
    expect(res.text).toBe('Hallooooo ulfaaaa!')
    done()
})
it('Call the /exabytes endpoint', async done => {
    const res = await request.get('/exabytes')
    expect(res.status).toBe(200)
    expect(res.text).toBe('Halo Exabytes Indonesia!')
    done()
})

  